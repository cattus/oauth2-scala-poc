

resolvers += Resolver.bintrayRepo("mattmoore", "bcrypt-scala")
resolvers += "Bartek's repo at Bintray" at "https://dl.bintray.com/btomala/maven"

val commonSettings = Seq(
  scalaVersion     := "2.12.8",
  version          := "0.1.0-SNAPSHOT",
  organization     := "com.example",
  organizationName := "example",
  maintainer := "cattum@gmail.com",
  scalacOptions ++= List(
    "-encoding", "UTF-8",
    "-target:jvm-1.8",
    "-unchecked",
    "-language:higherKinds",
    "-language:postfixOps",
    "-feature",
    "-Xlint:unused",
    "-Ywarn-unused:-implicits",
    "-Ypartial-unification"),
  javacOptions ++= Seq("-source", "1.8", "-target", "1.8"))


lazy val authorizationServer =
  (project in file("authorization-server"))
    .enablePlugins(SbtTwirl, LinuxPlugin, JavaServerAppPackaging, DockerPlugin)
    .settings(commonSettings)
    .settings(
      name := "authorization-server",
      packageName := "authorization-server",
      dockerExposedPorts ++= Seq(80),
      libraryDependencies ++= {
        import Dependencies._
        Seq(
          akkaCoreBundle,
          akkaHttpBundle,
          enumeratumBundle,
          postgresBundle,
          configBundle,
          dockerTestBundle,
          macwireBundle,
          loggingBundle,
          Seq(
            scalaTest,
            jwtCirce,
            jbcrypt,
            bouncyCastle)
        ).flatten
      }
    )
