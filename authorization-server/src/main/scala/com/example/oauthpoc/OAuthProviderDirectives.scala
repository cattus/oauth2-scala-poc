package com.example.oauthpoc


import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive1, ValidationRejection}
import akka.http.scaladsl.unmarshalling.Unmarshaller
import com.example.oauthpoc.models.{AccessTokenRequest, AuthorizationCodeStatement}



trait OAuthProviderDirectives {
  import com.example.oauthpoc.{OAuthParameterConstants => P}


  def withAccessTokenRequest: Directive1[AccessTokenRequest] =
    formField(P.GrantType).flatMap[Tuple1[AccessTokenRequest]] {
      case P.AuthorizationCode =>
        formFields(P.Code, P.ClientId, P.ClientSecret.?, P.CodeVerifier.?)
          .as(AccessTokenRequest.AuthorizationCode)
      case P.ClientCredentials =>
        formFields(P.ClientId, P.ClientSecret)
          .as(AccessTokenRequest.ClientCredentials)
      case P.Password =>
        formFields(P.Username, P.Password, P.ClientId.?, P.ClientSecret.?, P.Scope.as(ScopeUnmarshaller))
          .as(AccessTokenRequest.Password)
      case t => reject(ValidationRejection(s"Unsupported grant type: $t")) // TODO: maybe not the best rejection type
    }


  private def extractAllParameters: Directive1[Map[String, String]] =
    for {
      ps <- parameterMap.recover(_ => provide(Map.empty[String, String]))
      fs <- formFieldMap.recover(_ => provide(Map.empty[String, String]))
    } yield fs ++ ps


  def withAuthorizationCodeStatement: Directive1[AuthorizationCodeStatement] = {
    extractAllParameters.flatMap { parameters =>
      // TODO: better validation

      def require(p: String) = parameters.get(p).toRight(s"$p missing")
      def opt(p: String): Either[String, Option[String]] = Right(parameters.get(p))

      val eitherACS = for {
        clientId <- require(P.ClientId)
        redirectUri <- require(P.RedirectUri)
        scope <- opt(P.Scope).map(_.getOrElse("")).map(parseScope)
        state <- opt(P.State)
        codeChallenge <- opt(P.CodeChallenge)
        codeChallengeMethod <- opt(P.CodeChallengeMethod)
      }
        yield AuthorizationCodeStatement(
          clientId = clientId,
          redirectUri = redirectUri,
          scope = scope,
          state = state,
          codeChallenge = codeChallenge,
          codeChallengeMethod = codeChallengeMethod)

      eitherACS match {
        case Right(acs) => provide(acs)
        case Left(message) => reject(ValidationRejection(message))
      }
    }
  }

  private def parseScope(scope: String): Seq[String] = scope.split(' ').map(_.trim).filter(_.nonEmpty)

  def ScopeUnmarshaller[T]: Unmarshaller[String, Seq[String]] =
    Unmarshaller.strict[String, Seq[String]](parseScope)
}
