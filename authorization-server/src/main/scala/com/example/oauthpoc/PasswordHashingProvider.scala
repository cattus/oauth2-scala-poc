package com.example.oauthpoc

import com.example.oauthpoc.models.PasswordStoreConfig
import org.mindrot.jbcrypt.BCrypt



class PasswordHashingProvider(passwordStoreConfig: PasswordStoreConfig) {
  def hashPassword(userName: String, password: String): String =
    BCrypt.hashpw(
      password + userName + passwordStoreConfig.salt,
      BCrypt.gensalt(passwordStoreConfig.saltRounds))


  def checkPassword(userName: String, password: String, passwordHash: String): Boolean =
    BCrypt.checkpw(password + userName + passwordStoreConfig.salt, passwordHash)
}
