package com.example.oauthpoc.models

case class RegisterUserRequest(
    username: String,
    password: String)
