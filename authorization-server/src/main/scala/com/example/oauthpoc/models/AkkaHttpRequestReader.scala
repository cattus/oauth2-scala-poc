package com.example.oauthpoc.models


import akka.http.scaladsl.model.HttpRequest
import com.example.oauthpoc.models.Parameters.ParameterReadResult



class AkkaHttpRequestReader(request: HttpRequest) extends RawRequestReader {
  override def parameter[T](propertyHandler: Parameters.ParameterHandler[T]): ParameterReadResult[T] = ???
  override def parameterOpt[T](propertyHandler: Parameters.ParameterHandler[T]): ParameterReadResult[Option[T]] = ???
  override def authorizationHeader: Option[String] = ???
  override def authorizationToken: Option[String] = ???
}
