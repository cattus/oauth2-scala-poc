package com.example.oauthpoc.models

case class AuthorizationCodeRequest(
    authInfo: AuthInfo,
    codeStatement: AuthorizationCodeStatement)
