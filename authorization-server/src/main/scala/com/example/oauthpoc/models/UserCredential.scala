package com.example.oauthpoc.models

case class UserCredential(
    id: UserId,
    userName: String,
    passwordHash: String)


case class UserId(value: String) extends AnyVal