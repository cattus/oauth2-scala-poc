package com.example.oauthpoc.models


import com.example.oauthpoc.models.Parameters.{ParameterHandler, ParameterReadResult}



trait RawRequestReader {
  def parameter[T](propertyHandler: ParameterHandler[T]): ParameterReadResult[T]

  def parameterOpt[T](propertyHandler: ParameterHandler[T]): ParameterReadResult[Option[T]]

  def authorizationHeader: Option[String]

  def authorizationToken: Option[String]
}