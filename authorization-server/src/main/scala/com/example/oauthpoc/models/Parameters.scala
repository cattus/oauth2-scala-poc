package com.example.oauthpoc.models

import com.example.oauthpoc.models


// I supposed to implement the request parsing and handling in some common way at very start
// and implemented this stuff for fancy parameters handling.
// Then I decide to implement request handling closer to akka-http way
// but decide to leave this unused code here as the demonstration of approach
object Parameters {

  object Implicits {
    implicit class PropertyExtractorExt(private val properties: Map[String, String])
      extends AnyVal
    {
      def extract[T](propertyHandler: ParameterHandler[T]): ParameterReadResult[T] =
        extractOpt(propertyHandler).flatMap(_.toRight(ParameterReadError.NotFound(propertyHandler.name)))

      def extractOpt[T](propertyHandler: ParameterHandler[T]): ParameterReadResult[Option[T]] =
        Right(properties.get(propertyHandler.name)).flatMap {
          case Some(v) => propertyHandler.decode(v).map(Some(_))
          case None => Right(None)
        }
    }
  }

  sealed class ParameterReadError(parameter: String, message: String) extends Exception(message)

  object ParameterReadError {
    case class ParseError(parameter: String, details: String)
      extends ParameterReadError(parameter, s"Parameter parse error `$parameter`: $details")

    case class NotFound(parameter: String)
      extends ParameterReadError(parameter, s"Parameter `$parameter` not found")
  }

  type ParameterReadResult[T] = Either[ParameterReadError, T]

  trait ParameterHandler[T] { outer =>
    def name: String
    def decode(value: String): ParameterReadResult[T]
    def encode(value: T): String

    def apply(value: T): (String, String) = name -> encode(value)

    def xmap[B](map: T => ParameterReadResult[B], cmap: B => T): ParameterHandler[B] = new ParameterHandler[B] {
      override def name = outer.name
      override def decode(value: String) = outer.decode(value).flatMap(map)
      override def encode(value: B) = outer.encode(cmap(value))
    }
  }

  class StringParameter(val name: String) extends ParameterHandler[String] {
    override def decode(value: String): ParameterReadResult[String] = Right(value)
    override def encode(value: String): String = value
  }

  object StringParameter {
    def apply(name: String): StringParameter = new StringParameter(name)
  }

  object EnumParameter {
    import enumeratum._
    def apply[T <: EnumEntry](name: String, enum: Enum[T]): ParameterHandler[T] =
      StringParameter(name).xmap(
        enum.withNameOption(_).toRight(ParameterReadError.ParseError(name, "Illegal enum value")),
        _.entryName)
  }

  val AccessToken = StringParameter("access_token")
  val ClientId = StringParameter("client_id")
  val ClientSecret = StringParameter("client_secret")
  val Code = StringParameter("code")
  val GrantType = EnumParameter("grant_type", models.GrantType)
  val PKCEChallenge = StringParameter("code_challenge")
  val PKCEChallengeMethod = EnumParameter("code_challenge_method", models.PKCEChallengeMethod)
  val RedirectUri = StringParameter("redirect_url")
}