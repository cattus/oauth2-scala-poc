package com.example.oauthpoc.models

case class AccessTokenResponse(
    accessToken: String,
    tokenType: String,
    expiresIn: Long,
    refreshToken: Option[String])
