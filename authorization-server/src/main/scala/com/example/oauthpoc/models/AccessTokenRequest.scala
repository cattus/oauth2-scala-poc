package com.example.oauthpoc.models

sealed trait AccessTokenRequest

object AccessTokenRequest {
  case class AuthorizationCode(
      code: String,
      clientId: String,
      clientSecret: Option[String],
      codeVerifier: Option[String])
    extends AccessTokenRequest

  case class Password(
      username: String,
      password: String,
      clientId: Option[String] = None,
      clientSecret: Option[String] = None,
      scope: Seq[String] = Seq())
  extends AccessTokenRequest

  case class ClientCredentials(clientId: String, clientSecret: String)
    extends AccessTokenRequest
}
