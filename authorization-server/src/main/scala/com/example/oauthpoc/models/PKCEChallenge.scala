package com.example.oauthpoc.models

case class PKCEChallenge(method: PKCEChallengeMethod, code: String)
