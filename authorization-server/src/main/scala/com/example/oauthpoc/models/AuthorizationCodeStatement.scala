package com.example.oauthpoc.models

case class AuthorizationCodeStatement(
    clientId: String,
    redirectUri: String,
    scope: Seq[String],
    state: Option[String],
    codeChallenge: Option[String],
    codeChallengeMethod: Option[String])
