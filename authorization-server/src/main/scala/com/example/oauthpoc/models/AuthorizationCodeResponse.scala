package com.example.oauthpoc.models



case class AuthorizationCodeResponse(code: String)
