package com.example.oauthpoc.models

import enumeratum._
import enumeratum.EnumEntry.Snakecase


sealed trait GrantType extends EnumEntry with Snakecase

object GrantType extends Enum[GrantType] {
  case object AuthorizationCode extends GrantType
  case object ClientCredentials extends GrantType
  case object Password extends GrantType

  override def values = findValues
}
