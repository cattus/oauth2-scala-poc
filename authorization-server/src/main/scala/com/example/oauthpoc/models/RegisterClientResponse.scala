package com.example.oauthpoc.models

case class RegisterClientResponse(clientId: String)
