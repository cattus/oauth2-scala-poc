package com.example.oauthpoc.models

import enumeratum._

sealed trait PKCEChallengeMethod extends EnumEntry

object PKCEChallengeMethod extends Enum[PKCEChallengeMethod] {
  case object Plain extends PKCEChallengeMethod
  case object S256  extends PKCEChallengeMethod

  override def values = findValues
}
