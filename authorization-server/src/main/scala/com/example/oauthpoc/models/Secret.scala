package com.example.oauthpoc.models

case class Secret(value: String) {
  // to prevent accidental disclosure due to printing it out somewhere
  override def toString: String = s"SecretKey(..)"
}
