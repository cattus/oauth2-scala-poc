package com.example.oauthpoc.models

case class AuthInfo(
    clientId: Option[String] = None,
    userId: Option[String] = None,
    scope: Seq[String] = Seq.empty)
