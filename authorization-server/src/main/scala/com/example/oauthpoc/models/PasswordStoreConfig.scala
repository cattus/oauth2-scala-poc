package com.example.oauthpoc.models

case class PasswordStoreConfig(
    salt: String,
    saltRounds: Int,
)
