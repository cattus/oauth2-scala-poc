package com.example.oauthpoc.models

case class User(id: UserId, userName: String)
