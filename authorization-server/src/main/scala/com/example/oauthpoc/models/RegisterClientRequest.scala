package com.example.oauthpoc.models

case class RegisterClientRequest(
    publicKey: String,
    allowedUris: Seq[String])


