package com.example.oauthpoc.models

case class Client(
    id: ClientId,
    publicKey: String,
    allowedUris: Seq[String])


case class ClientId(value: String) extends AnyVal
