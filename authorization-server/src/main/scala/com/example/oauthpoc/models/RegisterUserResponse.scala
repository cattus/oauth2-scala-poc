package com.example.oauthpoc.models

case class RegisterUserResponse(
    userId: String,
    userName: String)
