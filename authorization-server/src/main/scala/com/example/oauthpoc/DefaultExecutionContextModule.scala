package com.example.oauthpoc


import scala.concurrent.ExecutionContext



trait DefaultExecutionContextModule {
  // we can redefine it later
  implicit lazy val executionContext: ExecutionContext = ExecutionContext.global
}
