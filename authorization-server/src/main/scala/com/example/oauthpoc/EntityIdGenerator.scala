package com.example.oauthpoc

trait EntityIdGenerator {
  def newId: String
}
