package com.example.oauthpoc


case class ResourceProviderConfig(
    authorizationIssuer: String,
    authorizationServerKey: String,
    issuer: String,
    publicKey: String,
    privateKey: String)
