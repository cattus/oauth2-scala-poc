package com.example.oauthpoc

class ApiError(message: String) extends Exception(message)

object ApiError {
  case class EntityAlreadyExists(message: String) extends ApiError(message)
}
