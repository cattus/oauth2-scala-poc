package com.example.oauthpoc


import com.example.oauthpoc.models.PasswordStoreConfig
import com.softwaremill.macwire._
import pureconfig._
import pureconfig.generic.auto._

trait AuthorizationServerModule
  extends DBContextModule
    with ActorSystemModule
    with ConfigModule
    with DefaultExecutionContextModule
{
  lazy val clockContext: ClockContext = ClockContext.utc
  lazy val oauthProviderConfig: OAuthProviderConfig = loadConfigOrThrow[OAuthProviderConfig](config, "oauth-provider")
  lazy val passwordStoreConfig: PasswordStoreConfig = loadConfigOrThrow[PasswordStoreConfig](config, "password-store")
  lazy val passwordHashProvider: PasswordHashingProvider = wire[PasswordHashingProvider]
  lazy val entityIdGenerator: EntityIdGenerator = EntityIdGeneratorUUID
  lazy val clientService: ClientService = wire[ClientServicePG]
  lazy val userService: UserService = wire[UserServicePG]
  lazy val oauthProviderService: OAuthProviderService = wire[OAuthProviderService]
  lazy val authorizationServerService: AuthorizationServerService = wire[AuthorizationServerService]
}
