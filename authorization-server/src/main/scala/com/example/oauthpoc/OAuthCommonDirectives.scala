package com.example.oauthpoc


import scala.concurrent.Future
import scala.util.{Failure, Success}

import akka.http.scaladsl.model.headers.{HttpChallenges, OAuth2BearerToken}
import akka.http.scaladsl.server.AuthenticationFailedRejection.CredentialsMissing
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{AuthenticationFailedRejection, Directive1}



trait OAuthCommonDirectives {
  import com.example.oauthpoc.{OAuthParameterConstants => P}

  private def rejectAuthenticationFailed(realm: String) =
    reject(AuthenticationFailedRejection(CredentialsMissing, HttpChallenges.oAuth2(realm)))

  def accessTokenFromForm(realm: String): Directive1[String] =
    formField(P.AccessToken).recover(_ => rejectAuthenticationFailed(realm))

  def accessTokenFromQueryParams(realm: String): Directive1[String] =
    parameters(P.AccessToken).recover(_ => rejectAuthenticationFailed(realm))

  def accessTokenFromAuthorizationHeader(realm: String): Directive1[String] =
    extractCredentials.flatMap {
      case Some(OAuth2BearerToken(token)) => provide(token)
      case _ => rejectAuthenticationFailed(realm)
    }

  def requireAccessToken(realm: String): Directive1[String] =
    accessTokenFromAuthorizationHeader(realm) |
      accessTokenFromForm(realm) |
      accessTokenFromQueryParams(realm)

  def extractAccessToken: Directive1[Option[String]] =
    requireAccessToken("").map(Some(_)) | provide(None)

  type Authenticator[T] = String => Future[Option[T]]

  def authenticate[T](realm: String, authenticator: Authenticator[T]): Directive1[T] =
    requireAccessToken(realm).map(authenticator).flatMap[Tuple1[T]] { auth =>
      onComplete(auth).flatMap {
        case Success(Some(v)) => provide(v)
        case Success(None) => rejectAuthenticationFailed(realm)
        case Failure(e) => failWith(e)
      }
    }
}

