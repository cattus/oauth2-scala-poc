package com.example.oauthpoc


sealed trait OAuthCodeFlowAction

object OAuthCodeFlowAction {
  case class RequireUserLogin(parameters: Map[String, String])
    extends OAuthCodeFlowAction

  case class RequireUserConsent(parameters: Map[String, String])
    extends OAuthCodeFlowAction

  case class ErrorCallback(
      error: OAuthError,
      state: String,
      redirectUri: String)
    extends OAuthCodeFlowAction

  case class CodeCallback(
      redirectUri: String,
      parameters: Map[String, String])
    extends OAuthCodeFlowAction

  case class RequestError(error: OAuthError)
    extends OAuthCodeFlowAction
}
