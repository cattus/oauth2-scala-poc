package com.example.oauthpoc


import scala.concurrent.Await

import scala.concurrent.duration._

import akka.stream.ActorMaterializer


object AuthorizationServiceApp
  extends App
  with AuthorizationServerModule
{
  val bindingFuture = authorizationServerService.bind(oauthProviderConfig.host, oauthProviderConfig.port)

  sys.addShutdownHook {
    Await.result(bindingFuture.flatMap(_.terminate(10 seconds)), 30 seconds)
  }
}
