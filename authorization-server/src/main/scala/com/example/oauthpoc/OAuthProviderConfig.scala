package com.example.oauthpoc


import scala.concurrent.duration.FiniteDuration



case class OAuthProviderConfig(
    issuer: String,
    publicKey: String,
    privateKey: String,
    realm: String,
    tokenExpirationTime: FiniteDuration,
    authorizationCodeExpirationTime: FiniteDuration,
    host: String,
    port: Int)