package com.example.oauthpoc


import java.util.UUID



object EntityIdGeneratorUUID extends EntityIdGenerator {
  override def newId: String = UUID.randomUUID.toString
}
