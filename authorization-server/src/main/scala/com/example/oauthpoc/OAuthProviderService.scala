package com.example.oauthpoc


import java.nio.charset.StandardCharsets
import java.security.{MessageDigest, SecureRandom}
import java.time.{Clock, Instant}
import java.util.Base64

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

import akka.actor.{Actor, ActorSystem, Props}
import akka.pattern.ask
import cats.implicits._
import cats.data._
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}
import io.circe._
import io.circe.generic.auto._
import io.circe.syntax._
import com.example.oauthpoc.models._
import org.apache.logging.log4j.scala.Logging



class OAuthProviderService(
    oauthProviderConfig: OAuthProviderConfig,
    userService: UserService,
    clientService: ClientService,
    clockContext: ClockContext,
    actorSystem: ActorSystem)
  extends OAuthServiceCommon[AuthInfo]
    with Logging
{
  import AuthorizationCodeStoreActor._
  import com.example.oauthpoc.{OAuthParameterConstants => P}

  private implicit val executionContext: ExecutionContext = actorSystem.dispatcher
  private val sha256Digest = MessageDigest.getInstance("SHA-256")
  private val base64Encoder = Base64.getUrlEncoder
  private val secureRandom = new SecureRandom()
  private val codeStoreActor = actorSystem.actorOf(Props(new AuthorizationCodeStoreActor(clockContext)))

  override protected implicit def clock: Clock = clockContext.clock

  override protected def authInfoJsonDecoder: Decoder[AuthInfo] = implicitly[Decoder[AuthInfo]]

  override protected def authorizationServerPublicKey: String = oauthProviderConfig.publicKey

  def issuer: String = oauthProviderConfig.issuer

  def realm: String = oauthProviderConfig.realm

  def createAuthorizationToken(authInfo: AuthInfo, expirationTime: FiniteDuration): String = {
    val claim = JwtClaim(authInfo.asJson.dropNullValues.toString)
      .expiresIn(expirationTime.toSeconds)
      .by(issuer)
      .issuedAt(clock.instant.getEpochSecond)
    JwtCirce.encode(claim, oauthProviderConfig.privateKey, JwtAlgorithm.RS256)
  }


  def handleAuthorizationCodeRequest(request: AuthorizationCodeRequest): Future[AuthorizationCodeResponse] =
    Future.unit.flatMap { _ =>
      import request._

      if (request.authInfo.userId.isEmpty)
        Future.failed(new OAuthError.AccessDenied("not a user"))
      else {
        for {
          client <- clientService.findById(ClientId(codeStatement.clientId))
            .map(_.getOrElse(throw new OAuthError.UnauthorizedClient("unknown client")))
          _ <- if (client.allowedUris.contains(codeStatement.redirectUri))
            Future.unit
          else Future.failed(new OAuthError.AccessDenied("not a user"))
          code <- createAuthorizationCode(request)
        } yield AuthorizationCodeResponse(code)
      }
    }


  def handleAccessTokenRequest(accessTokenRequest: AccessTokenRequest): Future[AccessTokenResponse] = {
    accessTokenRequest match {
      case r: AccessTokenRequest.AuthorizationCode => processAccessTokenRequest(r)
      case r: AccessTokenRequest.ClientCredentials => processAccessTokenRequest(r)
      case r: AccessTokenRequest.Password => processAccessTokenRequest(r)
        // TODO: refresh token
    }
  }


  private def processAccessTokenRequest(accessTokenRequest: AccessTokenRequest.Password): Future[AccessTokenResponse] = {
    OptionT.fromOption[Future](accessTokenRequest.clientId)
        .map(ClientId(_))
        .semiflatMap { clientId =>
          authorizeClientByUserPassword(
            clientId = clientId,
            accessTokenRequest.clientSecret.getOrElse(""),
            username = accessTokenRequest.username,
            password = accessTokenRequest.password,
            scope = accessTokenRequest.scope)
        }.getOrElseF {
      authorizeUserByUserPassword(
        username = accessTokenRequest.username,
        accessTokenRequest.password,
        accessTokenRequest.scope)
    }
  }


  private def authorizeClientByUserPassword(
      clientId: ClientId,
      clientSecret: String,
      username: String,
      password: String,
      scope: Seq[String]): Future[AccessTokenResponse] =
    for {
      client <- getVerifiedClient(clientId, clientSecret)
      maybeUser <- userService.findByCredentials(username, password)
      user = maybeUser.getOrElse(throw new OAuthError.AccessDenied())
    } yield
      createAccessTokenResponse(AuthInfo(
        clientId = Some(client.id.value),
        userId = Some(user.id.value),
        scope = scope // TODO: validate scope
      ))


  private def authorizeUserByUserPassword(
      username: String,
      password: String,
      scopes: Seq[String]): Future[AccessTokenResponse] =
    for {
      maybeUser <- userService.findByCredentials(username, password)
      user = maybeUser.getOrElse(throw new OAuthError.AccessDenied())
    } yield
      createAccessTokenResponse(AuthInfo(
        userId = Some(user.id.value),
        scope = scopes // TODO: validate scopes
      ))



  private def processAccessTokenRequest(
      accessTokenRequest: AccessTokenRequest.ClientCredentials): Future[AccessTokenResponse] =
    for {
      client <- getVerifiedClient(ClientId(accessTokenRequest.clientId), accessTokenRequest.clientSecret)
    } yield createAccessTokenResponse(AuthInfo(clientId = Some(client.id.value)))


  private def processAccessTokenRequest(
      accessTokenRequest: AccessTokenRequest.AuthorizationCode): Future[AccessTokenResponse] = {
    for {
      authorizationCodeRequest <- getAndDropAuthorizationCode(accessTokenRequest.code)
        .map(_.getOrElse(throw new OAuthError.AccessDenied("")))
      client <- getVerifiedClient(ClientId(accessTokenRequest.clientId), accessTokenRequest.clientSecret.getOrElse(""))
    } yield {
      val statement = authorizationCodeRequest.codeStatement
      val codeChallengeMethod = statement.codeChallengeMethod.getOrElse(P.Plain)
      val codeVerifier = accessTokenRequest.codeVerifier.getOrElse("")

      if (!statement.codeChallenge.forall(verifyCodeChallenge(_, codeChallengeMethod, codeVerifier)))
        throw new OAuthError.AccessDenied()

      val authInfo = AuthInfo(
        clientId = Some(client.id.value),
        userId = authorizationCodeRequest.authInfo.userId,
        scope = authorizationCodeRequest.codeStatement.scope)
      createAccessTokenResponse(authInfo)
    }
  }


  private def verifyCodeChallenge(codeChallenge: String, codeChallengeMethod: String, verifier: String): Boolean =
    codeChallengeMethod.toLowerCase match {
      case P.S256 => checkS256Code(codeChallenge, verifier)
      case _ => codeChallenge == verifier
    }


  private def getVerifiedClient(clientId: ClientId, clientSecret: String): Future[Client] =
    for {
      client <- clientService.findById(clientId).map(_.getOrElse(throw new OAuthError.AccessDenied()))
      _ <- if (checkClientSecret(client.id, clientSecret, client.publicKey))
        Future.unit
      else Future.failed(new OAuthError.AccessDenied())
    } yield client


  private def createAccessTokenResponse(authInfo: AuthInfo): AccessTokenResponse =
    AccessTokenResponse(
      accessToken = createAuthorizationToken(authInfo, oauthProviderConfig.tokenExpirationTime),
      tokenType = P.Bearer,
      expiresIn = oauthProviderConfig.tokenExpirationTime.toSeconds,
      refreshToken = None) // TODO: refreshToken implementation


  private def checkClientSecret(clientId: ClientId, secret: String, clientPublicKey: String): Boolean =
    JwtCirce
      .decode(secret, clientPublicKey, JwtAlgorithm.allAsymmetric())
      .toOption
      .exists(_.isValid(clientId.value))

  private def checkS256Code(code: String, verifier: String): Boolean =
    base64Encoder.encodeToString(
      sha256Digest.digest(verifier.getBytes(StandardCharsets.UTF_8))) == code


  private def createAuthorizationCode(request: AuthorizationCodeRequest): Future[String] = {
    val buffer = new Array[Byte](32)
    secureRandom.nextBytes(buffer)
    val code = base64Encoder.encodeToString(buffer)
    codeStoreActor ! RegisterCode(code, request, oauthProviderConfig.tokenExpirationTime)
    Future.successful(code)
  }


  private def getAndDropAuthorizationCode(code: String): Future[Option[AuthorizationCodeRequest]] = {
    // not the best way but ok here
    codeStoreActor.ask(GetAndDrop(code))(1 second)
      .mapTo[GetCodeResponse].map(_.request)
  }
}


// since akka 2.6 it's better to use typed actors
class AuthorizationCodeStoreActor(clockContext: ClockContext) extends Actor
{
  import AuthorizationCodeStoreActor._
  import context.become
  import context.dispatcher

  override def receive = work(Map.empty)

  private def work(codes: Map[String, AuthorizationRequestHolder]): Receive = {
    case RegisterCode(code, request, timeout) =>
      // TODO: improve code cleanup performance
      // Scheduling for each code is not efficient.
      // There are a lot of more efficient ways to clear outdated code.
      // For example we can create codes prefixed with the timestamp and store them in sorted order
      // using TreeSet or even TreeMap.
      // So we will able easily clear outdated codes when new one added or some code checked or removed.
      val codeHolder = AuthorizationRequestHolder(request, clockContext.instant.plusMillis(timeout.toMillis))
      become(work(codes + (code -> codeHolder)))
      context.system.scheduler.scheduleOnce(timeout, self, DropCode(code))

    case GetAndDrop(code) =>
      codes.get(code) match {
        case Some(found) if clockContext.instant.isBefore(found.expirationTime) =>
          sender ! GetCodeResponse(Some(found.request))
        case _ => sender ! GetCodeResponse(None)
      }

      become(work(codes - code))

    case DropCode(code) =>
      become(work(codes - code))
  }
}


private object AuthorizationCodeStoreActor {
  case class RegisterCode(code: String, request: AuthorizationCodeRequest, timeout: FiniteDuration)
  case class GetAndDrop(code: String)
  case class GetCodeResponse(request: Option[AuthorizationCodeRequest])
  case class DropCode(code: String)
  private[AuthorizationCodeStoreActor] case class AuthorizationRequestHolder(
      request: AuthorizationCodeRequest,
      expirationTime: Instant)
}