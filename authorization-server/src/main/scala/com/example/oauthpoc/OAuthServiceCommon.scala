package com.example.oauthpoc


import java.time.Clock

import pdi.jwt.{JwtAlgorithm, JwtCirce}
import io.circe._


trait OAuthServiceCommon[AuthInfoType] {
  protected implicit def clock: Clock

  protected def authInfoJsonDecoder: Decoder[AuthInfoType]

  def issuer: String

  protected def authorizationServerPublicKey: String

  def extractAuthInfo(token: String): Option[AuthInfoType] =
    JwtCirce
      .decode(token, authorizationServerPublicKey, JwtAlgorithm.allAsymmetric())
      .toOption
      .filter(_.isValid(issuer))
      .map(_.content)
      .flatMap(parser.parse(_).toOption)
      .flatMap(_.as(authInfoJsonDecoder).toOption)
}
