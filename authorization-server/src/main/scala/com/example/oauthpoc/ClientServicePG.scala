package com.example.oauthpoc
import scala.concurrent.{ExecutionContext, Future}

import com.example.oauthpoc.db.DBContext
import com.example.oauthpoc.models.{Client, ClientId}



class ClientServicePG(
    entityIdGenerator: EntityIdGenerator,
    dbContext: DBContext,
    implicit protected val executionContext: ExecutionContext)
  extends ClientService
{
  import dbContext._

  override def findById(clientId: ClientId): Future[Option[Client]] = {
    dbContext.run {
      query[Client].filter(_.id == lift(clientId)).take(1)
    }.map(_.headOption)
  }


  override def register(publicKey: String, allowedUris: Seq[String]): Future[Client] = {
    // TODO: URI validation

    val client = Client(
      id = ClientId(entityIdGenerator.newId),
      publicKey = publicKey,
      allowedUris = allowedUris)

    dbContext.run {
      query[Client].insert(lift(client))
    }.map(_ => client)
  }
}
