package com.example.oauthpoc

import java.time._


trait ClockContext {
  def clock: Clock

  def instant: Instant = clock.instant()
}


object ClockContext {
  val defaultTimeZone: ClockContext = new ProxyClockContext(Clock.systemDefaultZone)

  val utc: ClockContext = new ProxyClockContext(Clock.systemUTC)
}


class ProxyClockContext(override val clock: Clock) extends ClockContext