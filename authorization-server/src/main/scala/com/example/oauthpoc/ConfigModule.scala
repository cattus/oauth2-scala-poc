package com.example.oauthpoc


import com.typesafe.config.{Config, ConfigFactory}



trait ConfigModule {
  lazy val config: Config = loadConfig

  def loadConfig: Config = ConfigFactory.load
}
