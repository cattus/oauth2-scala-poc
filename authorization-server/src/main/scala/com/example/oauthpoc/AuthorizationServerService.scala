package com.example.oauthpoc


import java.security.Security

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{StatusCodes, Uri}
import akka.http.scaladsl.server.Route
import akka.stream.{ActorMaterializer, Materializer}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.unmarshalling.Unmarshaller
import com.example.oauthpoc.AuthorizationServiceApp.authorizationServerService
import com.example.oauthpoc.models.{AuthInfo, AuthorizationCodeRequest, RegisterClientRequest, RegisterClientResponse, RegisterUserRequest, RegisterUserResponse}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import org.apache.logging.log4j.scala.Logging



class AuthorizationServerService(
    clientService: ClientService,
    userService: UserService,
    oauthProviderService: OAuthProviderService,
    protected implicit val actorSystem: ActorSystem)
  extends FailFastCirceSupport
    with OAuthCommonDirectives
    with OAuthProviderDirectives
    with Logging
{
  import Unmarshaller._
  import com.example.oauthpoc.{OAuthParameterConstants => P}

  // TODO: add if not exists
  Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider())

  protected implicit val materializer: Materializer = ActorMaterializer()
  protected implicit val executionContext: ExecutionContext = actorSystem.dispatcher

  protected val realm: String = oauthProviderService.realm

  val route: Route = concat(
    (pathPrefix("status") & pathEndOrSingleSlash & get) {
      complete("OK")
    },
    pathPrefix("oauth") {
      concat(
        path("code") {
          get {
            withAuthorizationCodeStatement { codeStatement =>
              complete(codeStatement) // redirect to authorization and consent approve
            }
          }
        },
        (path("token") & post) {
          withAccessTokenRequest { request =>
            onComplete(oauthProviderService.handleAccessTokenRequest(request)) {
              case Success(response) => complete(response)
              case Failure(e: OAuthError) =>
                complete(StatusCodes.Unauthorized -> e.name)
              case Failure(e) =>
                logger.error("Error processing request", e)
                complete(StatusCodes.InternalServerError)
            }
          }
        },
        pathPrefix("consent") {
          concat(
            (pathEndOrSingleSlash & get) {
              complete("consent page stub")
            },
            (path("approve") & post) { // TODO: secure CORS
              withAuthorizationCodeStatement { codeStatement =>
                authorizeForConsentApprove { authInfo =>
                  val uri = Uri(codeStatement.redirectUri)

                  onComplete(
                    oauthProviderService.handleAuthorizationCodeRequest(
                      AuthorizationCodeRequest(authInfo, codeStatement))) {
                    case Success(response) =>
                      redirect(uri.withQuery(Query(
                        Seq(P.Code -> response.code) ++
                          codeStatement.state.map(P.State -> _).toSeq :_*
                      )), StatusCodes.SeeOther)
                    case Failure(e: OAuthError) =>
                      redirect(uri.withQuery(Query(P.Error -> e.name)), StatusCodes.SeeOther)
                    case Failure(_) =>
                      // TODO: logging
                      redirect(uri.withQuery(Query(P.Error -> P.InternalError)), StatusCodes.SeeOther)
                  }
                }
              }
            }
          )
        }
      )
    },
    pathPrefix("api") {
      concat(
        (path("client") & put) {
          entity(as[RegisterClientRequest]) { request =>
            onSuccess(handleRegisterClientRequest(request))(complete(_))
          }
        },
        (path("user") & put) {
          entity(as[RegisterUserRequest]) { request =>
            onSuccess(handleRegisterUserRequest(request))(complete(_))
          }
        }
      )
    }
  )

  private def authorizeForConsentApprove =
    authenticate[AuthInfo](realm, token => Future.successful(oauthProviderService.extractAuthInfo(token)))

  private def handleRegisterClientRequest(request: RegisterClientRequest): Future[RegisterClientResponse] =
    clientService.register(request.publicKey, request.allowedUris)
      .map(_.id.value)
      .map(RegisterClientResponse(_))

  private def handleRegisterUserRequest(request: RegisterUserRequest): Future[RegisterUserResponse] =
    userService.register(request.username, request.password).map { user =>
      RegisterUserResponse(user.id.value, user.userName)
    }


  def bind(host: String, port: Int): Future[Http.ServerBinding] =
    Http().bindAndHandle(authorizationServerService.route, host, port)
}
