package com.example.oauthpoc.db


import com.typesafe.config.Config
import io.getquill.{PostgresAsyncContext, SnakeCase}



class DBContext(config: Config) extends PostgresAsyncContext(SnakeCase, config)
