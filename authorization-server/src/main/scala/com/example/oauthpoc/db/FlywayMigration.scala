package com.example.oauthpoc.db


import org.apache.logging.log4j.scala.Logging
import org.flywaydb.core.Flyway



object FlywayMigration extends Logging {
  private[this] def newFlyway(url: String, user: String, password: String): Flyway =
    Flyway.configure
      .dataSource(url, user, password)
      .schemas("public")
      .outOfOrder(true)
      .load

  def migrate(url: String, user: String, password: String): Unit =
    try {
      val flyway = newFlyway(url, user, password)
      flyway.clean()
      flyway.migrate()
      logger.info("db migration complete")
    } catch {
      case e: Exception =>
        logger.error(s"Error happened during in flyway migration: ${e.getMessage}", e)
        throw e
    }
}
