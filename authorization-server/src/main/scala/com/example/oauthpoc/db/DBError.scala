package com.example.oauthpoc.db


import com.github.mauricio.async.db.postgresql.exceptions.GenericDatabaseException
import com.github.mauricio.async.db.postgresql.messages.backend.{ErrorMessage, InformationMessage}

object DBError {
  // it's better to put another abstraction above the ErrorMessage here
  // to avoid coupling on concrete database driver implementation
  // but for now it's senseless here and will lead to unnecessary complexity
  def unapply(exception: Throwable): Option[ErrorMessage] = exception match {
    case e: GenericDatabaseException => Some(e.errorMessage)
    case _ => None
  }

  object AlreadyExistsError extends SQLState("23505")
}


sealed abstract class SQLState(val sqlStateValue: String) {
  @inline private[this] def checkStateValue(errorMessage: ErrorMessage): Boolean =
    errorMessage.fields.get(InformationMessage.SQLState).contains(sqlStateValue)

  def unapply(exception: Throwable): Option[ErrorMessage] =
    DBError.unapply(exception).filter(checkStateValue)
}
