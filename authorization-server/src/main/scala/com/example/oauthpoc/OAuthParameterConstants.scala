package com.example.oauthpoc

object OAuthParameterConstants {
  val AccessToken = "access_token"
  val GrantType = "grant_type"
  val AuthorizationCode = "authorization_code"
  val ClientCredentials = "client_credentials"
  val Password = "password"
  val ClientId = "client_id"
  val ClientSecret = "client_secret"
  val Username = "username"
  val Code = "code"
  val Scope = "scope"
  val CodeVerifier = "code_verifier"
  val RedirectUri = "redirect_uri"
  val State = "state"
  val CodeChallenge = "code_challenge"
  val CodeChallengeMethod = "code_challenge_method"
  val Plain = "plain"
  val S256 = "s256"
  val Bearer = "Bearer"
  val Error = "error"
  val InternalError = "internal_error"
  val AccessDenied = "access_denied"
  val UnauthorizedClient = "unauthorized_client"
}
