package com.example.oauthpoc


import akka.actor.ActorSystem



trait ActorSystemModule extends ConfigModule {
  implicit lazy val actorSystem: ActorSystem = ActorSystem("oauth-poc", config)
}
