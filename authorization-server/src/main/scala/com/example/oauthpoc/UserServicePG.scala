package com.example.oauthpoc

import scala.concurrent.{ExecutionContext, Future}

import com.example.oauthpoc.db.{DBContext, DBError}
import com.example.oauthpoc.models.{User, UserCredential, UserId}



class UserServicePG(
    entityIdGenerator: EntityIdGenerator,
    passwordHashingProvider: PasswordHashingProvider,
    dbContext: DBContext,
    implicit protected val executionContext: ExecutionContext)
  extends UserService
{
  import dbContext._
  import passwordHashingProvider.{checkPassword, hashPassword}

  override def findUserById(userId: UserId): Future[Option[User]] = {
    dbContext.run {
      query[UserCredential].filter(_.id == lift(userId)).take(1)
    }.map(_.headOption.map(toUser))
  }


  override def findUserByUserName(userName: String): Future[Option[User]] = {
    dbContext.run {
      query[UserCredential].filter(_.userName == lift(userName)).take(1)
    }.map(_.headOption.map(toUser))
  }


  override def register(userName: String, password: String): Future[User] = {
    val credential = UserCredential(
      id = UserId(entityIdGenerator.newId),
      userName = userName,
      passwordHash = hashPassword(userName, password))
    dbContext.run {
      query[UserCredential].insert(lift(credential))
    }.map(_ => toUser(credential))
  }.recoverWith {
    case DBError.AlreadyExistsError(_) =>
      Future.failed(ApiError.EntityAlreadyExists("user with such name already exists"))
  }


  override def findByCredentials(userName: String, password: String): Future[Option[User]] = {
    dbContext.run {
      query[UserCredential].filter(_.userName == lift(userName)).take(1)
    }.map(_.headOption.filter(checkCredential(password, _)).map(toUser))
  }


  private def toUser(userCredentials: UserCredential): User =
    User(
      id = userCredentials.id,
      userName = userCredentials.userName)


  private def checkCredential(password: String, userCredential: UserCredential): Boolean =
    checkPassword(userCredential.userName, password, userCredential.passwordHash)
}
