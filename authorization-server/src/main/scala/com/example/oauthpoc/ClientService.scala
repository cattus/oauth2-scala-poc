package com.example.oauthpoc


import scala.concurrent.Future

import com.example.oauthpoc.models.{Client, ClientId}



trait ClientService {
  def findById(clientId: ClientId): Future[Option[Client]]
  def register(publicKey: String, allowedUris: Seq[String]): Future[Client]
}
