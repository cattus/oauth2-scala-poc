package com.example.oauthpoc

trait AuthorizationModule {
  def userService: UserService
  def clientService: ClientService
  def oauthProviderService: OAuthProviderService
}
