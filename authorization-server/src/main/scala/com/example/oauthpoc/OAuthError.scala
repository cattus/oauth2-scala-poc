package com.example.oauthpoc

sealed class OAuthError(val name: String, message: String) extends Exception(message)


object OAuthError {
  class InvalidRequest(message: String) extends OAuthError("invalid_request", message)

  class AccessDenied(message: String) extends OAuthError("access_denied", message) {
    def this() = this("")
  }

  class UnauthorizedClient(message: String) extends OAuthError("unauthorized_client", message)

  class UnsupportedResponseType(message: String) extends OAuthError(name = "unsupported_response_type", message)

  class InvalidScope(message: String) extends OAuthError("invalid_scope", message)

  class InternalError(message: String) extends OAuthError("internal_error", message)
}
