package com.example.oauthpoc


import com.example.oauthpoc.db.DBContext



trait DBContextModule extends ConfigModule {
  lazy val dbContext: DBContext = new DBContext(config.getConfig("db-context"))
}
