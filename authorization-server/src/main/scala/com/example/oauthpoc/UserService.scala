package com.example.oauthpoc


import scala.concurrent.Future

import com.example.oauthpoc.models.{User, UserId}



trait UserService {
  def findUserById(userId: UserId): Future[Option[User]]
  def findUserByUserName(userName: String): Future[Option[User]]
  def register(userName: String, password: String): Future[User]
  def findByCredentials(userName: String, password: String): Future[Option[User]]
}
