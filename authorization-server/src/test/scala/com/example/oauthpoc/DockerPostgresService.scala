package com.example.oauthpoc


import java.sql.DriverManager

import com.whisk.docker._
import org.slf4j.LoggerFactory

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._

trait DockerPostgresService extends DockerKit {
  private[this] val connectionTimeout = 15 seconds

  def postgresAdvertisedPort = 5432
  def postgresUser = "postgres"
  def postgresPassword = ""

  val postgresContainer = DockerContainer("postgres:11.5-alpine")
    .withPorts(postgresAdvertisedPort -> None)
    .withEnv(s"POSTGRES_USER=$postgresUser", s"POSTGRES_PASSWORD=$postgresPassword")
    .withCommand("-c", "max_connections=100")
    .withReadyChecker(
      new PostgresReadyChecker(postgresUser, postgresPassword, postgresAdvertisedPort)
        .looped(Math.max(connectionTimeout.toSeconds.toInt, 1), 1.second)
    )

  lazy val postgresPort = Await.result(postgresContainer.getPorts(), connectionTimeout)(postgresAdvertisedPort)

  abstract override def dockerContainers: List[DockerContainer] =
    postgresContainer :: super.dockerContainers
}


class PostgresReadyChecker(user: String, password: String, advertisedPort: Int) extends DockerReadyChecker {
  private[this] val logger = LoggerFactory.getLogger(getClass)

  Class.forName("org.postgresql.Driver")

  override def apply(container: DockerContainerState)(implicit docker: DockerCommandExecutor,
      executor: ExecutionContext): Future[Boolean] =
    container
      .getPorts()
      .map { ports =>
        val url = s"jdbc:postgresql://${docker.host}:${ports(advertisedPort)}/"
        logger.debug(s"Trying to rich $url")
        val maybeConnection = Option(DriverManager.getConnection(url, user, password))
        maybeConnection.foreach(_.close())
        maybeConnection.isDefined
      }
}

