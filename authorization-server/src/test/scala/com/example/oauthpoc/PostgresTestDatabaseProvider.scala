package com.example.oauthpoc


import java.sql.DriverManager
import java.util.UUID

import scala.util.control.NonFatal

import com.example.oauthpoc.db.FlywayMigration
import com.whisk.docker.impl.spotify.DockerKitSpotify
import org.apache.logging.log4j.scala.Logging



object PostgresTestDatabaseProvider extends Logging {
  case class DatabaseInfo(
      host: String,
      port: Int,
      user: String,
      password: String,
      database: String)
  {
    def connectionUri = s"jdbc:postgresql://$host:$port/"

    def databaseUri = s"$connectionUri$database"
  }

  lazy val dockerService = {
    val s = new DockerPostgresService with DockerKitSpotify
    s.startAllOrFail()
    s
  }

  def newDatabaseInfo = DatabaseInfo(
    host = "localhost",
    port = dockerService.postgresPort,
    user = dockerService.postgresUser,
    password = dockerService.postgresPassword,
    database = s"test-${UUID.randomUUID}")

  def createDatabase: DatabaseInfo = {
    val info = newDatabaseInfo

    try {
//      Class.forName("org.postgresql.Driver")
      val connection = DriverManager.getConnection(info.connectionUri, info.user, info.password)

      try {
        connection.createStatement().execute(s"""CREATE DATABASE "${info.database}"""")
      } finally {
        connection.close()
      }

      FlywayMigration.migrate(info.databaseUri, info.user, info.password)

      logger.info(s"Database created: $info")
      info
    } catch {
      case NonFatal(e) =>
        logger.error(s"Failed to create DB: $info")
        throw e
    }
  }
}
