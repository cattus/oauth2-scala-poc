package com.example.oauthpoc


import java.util.UUID

import akka.http.scaladsl.model.FormData
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{FreeSpec, Matchers}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._



class OAuthCommonDirectivesTest
  extends FreeSpec
    with OAuthCommonDirectives
    with Matchers
    with ScalatestRouteTest
{
  private val AccessTokenParameter = "access_token"

  def checkToken(route: Route, path: String): Unit = {
    "should extract the token from Authorization header" in {
      val sample = UUID.randomUUID.toString
      Get(s"/$path?$AccessTokenParameter=wrong")
        .withHeaders(Authorization(OAuth2BearerToken(sample))) ~> route ~> check {
        responseAs[String] shouldEqual sample
      }
    }

    "should extract the token from form data" in {
      val sample = UUID.randomUUID.toString
      Post(s"/$path?$AccessTokenParameter=wrong")
        .withEntity(FormData(AccessTokenParameter -> sample).toEntity) ~> route ~> check {
        responseAs[String] shouldEqual sample
      }
    }

    "should extract the token from query parameters" in {
      val sample = UUID.randomUUID.toString
      Get(s"/$path?$AccessTokenParameter=$sample") ~> route ~> check {
        responseAs[String] shouldEqual sample
      }
    }
  }

  "OAuthCommonDirectives" - {

    "requireAccessToken" - {
      val Path = "path"

      val route =
        path(Path) {
          requireAccessToken("realm")(complete(_))
        }

      checkToken(route, Path)

      "should reject with Unauthorized when the token in not provided" in {
        Get(s"/$Path") ~> route ~> check {
          rejection shouldBe a[AuthenticationFailedRejection]
        }

        Post(s"/$Path") ~> route ~> check {
          rejection shouldBe a[AuthenticationFailedRejection]
        }
      }
    }

    "extractAccessToken" - {
      val NoTokenString  = "no-token"
      val Path = "path"

      val route = path(Path) {
        extractAccessToken {
          case Some(token) => complete(token)
          case None => complete(NoTokenString)
        }
      }

      checkToken(route, Path)

      "should properly handle the case when token was not provided" in {
        Post(s"/$Path") ~> route ~> check {
          responseAs[String] shouldEqual NoTokenString
        }
      }
    }
  }
}