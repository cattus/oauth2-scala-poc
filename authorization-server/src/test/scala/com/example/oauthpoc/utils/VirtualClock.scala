package com.example.oauthpoc.utils


import java.time.temporal.{ChronoUnit, TemporalAdjuster}
import java.time.{Clock, Duration, Instant, ZoneId, ZonedDateTime}
import java.util.concurrent.atomic.AtomicLong

import scala.concurrent.duration.FiniteDuration

import org.apache.logging.log4j.scala.Logging



object VirtualClock {
  def runningClock = new VirtualClock(Clock.systemDefaultZone)

  def runningClockUTC = new VirtualClock(Clock.systemUTC)

  def fixedClock = new VirtualClock(Clock.fixed(Clock.systemDefaultZone.instant, ZoneId.systemDefault))

  def fixedClockUTC = new VirtualClock(Clock.fixed(Clock.systemUTC.instant, ZoneId.systemDefault))
}


final class VirtualClock(private[this] val baseClock: Clock) extends Clock with Logging { self =>
  private[this] val offset = new AtomicLong(0)

  override def getZone = baseClock.getZone

  override def withZone(zone: ZoneId) = new Clock {
    override def getZone: ZoneId = zone
    override def withZone(zone: ZoneId): Clock = self.withZone(zone)
    override def instant: Instant = self.instant
  }

  override def instant: Instant = baseClock.instant().plus(offset.get(), ChronoUnit.MILLIS)

  def minimumAdvanceStep: Duration = Duration.ofMillis(1)

  def advance(step: Duration): Unit = {
    require(step.abs().compareTo(minimumAdvanceStep) >= 0, s"minimum supported step is $minimumAdvanceStep")
    val oldInstant = instant
    offset.addAndGet(step.toMillis)
    logger.info(s"Virtual system clock adjusted by $step from $oldInstant to $instant; offset = ${offset.get}")
  }

  def advance(step: FiniteDuration): Unit =
    advance(Duration.ofNanos(step.toNanos))

  def advanceTo(time: ZonedDateTime): Unit =
    advance(Duration.between(ZonedDateTime.now(this), time))

  def advanceTo(adjuster: TemporalAdjuster): Unit =
    advanceTo(ZonedDateTime.now(this).`with`(adjuster))

  override def toString: String = s"${getClass.getSimpleName}(${baseClock.instant()} +$offset)"
}
