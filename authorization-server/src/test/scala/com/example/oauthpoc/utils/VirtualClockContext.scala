package com.example.oauthpoc.utils


import java.time.temporal.{TemporalAdjuster, TemporalAdjusters}
import java.time.{DayOfWeek, Duration, ZonedDateTime}

import scala.concurrent.duration.FiniteDuration

import com.example.oauthpoc.ClockContext



class VirtualClockContext extends ClockContext {
  override val clock: VirtualClock = VirtualClock.runningClockUTC

  def advance(step: Duration): Unit = clock.advance(step)

  def advance(step: FiniteDuration): Unit = clock.advance(step)

  def advanceTo(time: ZonedDateTime): Unit = clock.advanceTo(time)

  def advanceTo(adjuster: TemporalAdjuster): Unit = clock.advanceTo(adjuster)

  def advanceToNext(dayOfWeek: DayOfWeek): Unit = clock.advanceTo(TemporalAdjusters.next(dayOfWeek))
}
