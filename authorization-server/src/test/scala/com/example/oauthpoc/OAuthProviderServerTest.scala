package com.example.oauthpoc


import java.security.MessageDigest
import java.time.Clock
import java.util.{Base64, UUID}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.Random

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes.Redirection
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.model.{FormData, StatusCodes, Uri}
import akka.http.scaladsl.server.AuthenticationFailedRejection
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{FreeSpec, Matchers}
import com.example.oauthpoc.models._
import com.typesafe.config.{Config, ConfigFactory}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import com.example.oauthpoc.utils.VirtualClockContext
import io.circe._
import io.circe.generic.auto._
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}
import pureconfig._
import pureconfig.generic.auto._

class OAuthProviderServerTest
  extends FreeSpec
    with Matchers
    with ScalatestRouteTest
    with FailFastCirceSupport
    with AuthorizationServerModule
{
  import com.example.oauthpoc.{OAuthParameterConstants => P}

  override lazy val clockContext = new VirtualClockContext
  implicit def clock: Clock = clockContext.clock
  override implicit lazy val actorSystem: ActorSystem = system
  private val sha256Digest = MessageDigest.getInstance("SHA-256")
  private val base64Encoder = Base64.getUrlEncoder

  lazy val databaseInfo = PostgresTestDatabaseProvider.createDatabase
  lazy val resourceProviderConfig: ResourceProviderConfig = loadConfigOrThrow[ResourceProviderConfig](config, "resource-provider")

  private val resourceProvider = new OAuthServiceCommon[AuthInfo] {
    override protected implicit def clock: Clock = clockContext.clock
    override protected def authInfoJsonDecoder: Decoder[AuthInfo] = implicitly[Decoder[AuthInfo]]
    override def issuer = oauthProviderConfig.issuer
    override protected def authorizationServerPublicKey = oauthProviderConfig.publicKey
  }

  private def extractAuthInfo(token: String): Option[AuthInfo] = resourceProvider.extractAuthInfo(token)


  override def loadConfig: Config = {
    import databaseInfo._
    ConfigFactory.parseString(
      s"""
         |db-context {
         |  host = "$host"
         |  port = $port
         |  user = "$user"
         |  password = "$password"
         |  database = "$database"
         |}
          """.stripMargin).withFallback(super.loadConfig)
  }

  private val clientPrivateKey = config.getString("client.private-key")
  private val clientPublicKey = config.getString("client.public-key")

  val route = authorizationServerService.route
  val appRedirectUri = "myapplication://authorization/callback"

  private def mkClientSecret(clientId: String): String = {
    val claim = JwtClaim().by(clientId)
    JwtCirce.encode(claim, clientPrivateKey, JwtAlgorithm.RS256)
  }


  private def registerUser: (String, String) = {
    val username = UUID.randomUUID.toString
    val password = UUID.randomUUID.toString

    Put(s"/api/user",  RegisterUserRequest(username, password)) ~> route ~> check {
      status shouldBe StatusCodes.OK
      val response = entityAs[RegisterUserResponse]
      response.userName shouldEqual username
    }

    username -> password
  }


  private def registerClient: (String, String) = {
    var clientId: String = ""

    Put(s"/api/client", RegisterClientRequest(clientPublicKey, Seq(appRedirectUri))) ~>
      route ~> check {
      status shouldBe StatusCodes.OK
      val entity = entityAs[RegisterClientResponse]
      clientService.findById(ClientId(entity.clientId)).sync shouldBe
        Some(Client(ClientId(entity.clientId), clientPublicKey, Seq(appRedirectUri)))
      clientId = entity.clientId
    }

    clientId -> mkClientSecret(clientId)
  }


  private def authorizeUser(username: String, password: String): String = {
    var accessToken = ""
    Post("/oauth/token").withEntity(
      FormData(
        P.GrantType -> P.Password,
        P.Username -> username,
        P.Password -> password,
        P.Scope -> "").toEntity) ~>
      route ~> check {
      status shouldBe StatusCodes.OK
      val response = entityAs[AccessTokenResponse]
      accessToken = response.accessToken
    }

    accessToken
  }


  private def requestAuthorizationCode(
      username: String,
      password: String,
      clientId: String,
      requestParams: Seq[(String, String)] = Seq.empty): Map[String, String] = {
    val accessToken = authorizeUser(username, password)
    requestAuthorizationCodeRaw(
      Seq(P.AccessToken -> accessToken,
        P.ClientId -> clientId,
        P.RedirectUri -> appRedirectUri) ++ requestParams)
  }


  private def requestAuthorizationCodeRaw(requestParams: Seq[(String, String)]): Map[String, String] = {
    var responseParams: Map[String, String] = Map.empty

    Post("/oauth/consent/approve").withEntity(
      FormData(requestParams :_*).toEntity) ~>
      route ~> check {
      checkRedirect(StatusCodes.SeeOther, appRedirectUri)
      responseParams = redirectUriParameters
    }

    responseParams
  }


  private def checkRedirect(redirection: Redirection, redirectUri: String) = {
    val redirectUriSample = Uri(redirectUri)
    status shouldBe redirection
    val maybeLocation = header[Location]
    val uri = maybeLocation.get.uri
    uri.scheme shouldBe redirectUriSample.scheme
    uri.authority shouldBe redirectUriSample.authority
    uri.path shouldBe redirectUriSample.path
    uri.rawQueryString shouldBe defined
  }


  private def redirectUriParameters: Map[String, String] =
    header[Location]
      .flatMap(_.uri.rawQueryString)
      .map(Query(_))
      .map(_.toMap)
      .getOrElse(Map.empty)


  // I prefer integration tests that should talk to server with true http requests and responses.
  // In ideal case tests should acts like actors interacting with the server to check it behavior.
  // In such scenarios most of all checks in my opinion
  // should be implemented without accessing internal components directly.
  // Here I use internal components like ClientService to check requests effects because I limited in time.
  "AuthorizationServer" - {

    // TODO: better to use separate tests suites on registration and authorization behavior
    "user management" - {
      "should register new user" in {
        Put(s"/api/user", RegisterUserRequest("name", "password")) ~> route ~> check {
          status shouldBe StatusCodes.OK
          val response = entityAs[RegisterUserResponse]
          response.userName shouldEqual "name"
        }
      }


      "should register new client" in {
        Put(s"/api/client", RegisterClientRequest(clientPublicKey, Seq(appRedirectUri))) ~>
          route ~> check {
          status shouldBe StatusCodes.OK
          val entity = entityAs[RegisterClientResponse]
          clientService.findById(ClientId(entity.clientId)).sync shouldBe
            Some(Client(ClientId(entity.clientId), clientPublicKey, Seq(appRedirectUri)))
        }
      }
    }


    "authorization" - {
      s"${P.GrantType} = ${P.Password}" - {
        "should grant authorization for user on behalf of itself" in {
          val (username, password) = registerUser
          val scope                = Seq("a", "b")

          Post("/oauth/token").withEntity(
            FormData(
              P.GrantType -> P.Password,
              P.Username -> username,
              P.Password -> password,
              P.Scope -> scope.mkString(" ")).toEntity) ~>
            route ~> check {

            status shouldBe StatusCodes.OK
            val response   = entityAs[AccessTokenResponse]
            val maybeToken = extractAuthInfo(response.accessToken)
            maybeToken shouldBe defined
            maybeToken.get.userId shouldBe defined
            maybeToken.get.clientId shouldBe empty
          }
        }


        "should grant authorization for client on behalf of the user" in {
          val (username, password)     = registerUser
          val (clientId, clientSecret) = registerClient
          val scope                    = Seq("a", "b")

          Post("/oauth/token").withEntity(
            FormData(
              P.GrantType -> P.Password,
              P.Username -> username,
              P.Password -> password,
              P.ClientId -> clientId,
              P.ClientSecret -> clientSecret,
              P.Scope -> scope.mkString(" ")).toEntity) ~>
            route ~> check {

            status shouldBe StatusCodes.OK
            val response   = entityAs[AccessTokenResponse]
            val maybeToken = extractAuthInfo(response.accessToken)
            maybeToken shouldBe defined
            maybeToken.get.userId shouldBe defined
            maybeToken.get.clientId shouldBe Some(clientId)

          }
        }


        "should not grant authorization when user credentials are wrong" in pending

        "should not grant authorization when client credentials are wrong" in pending

        // TODO: negative scenarios
      }

      s"${P.GrantType} = ${P.ClientCredentials}" - {
        "should authorize client by secret and provide access token on behalf of the client itself" in {
          val (clientId, clientSecret) = registerClient
          Post("/oauth/token").withEntity(
            FormData(
              P.GrantType -> P.ClientCredentials,
              P.ClientId -> clientId,
              P.ClientSecret -> clientSecret).toEntity) ~>
            route ~> check {

            status shouldBe StatusCodes.OK
            val response   = entityAs[AccessTokenResponse]
            val maybeToken = extractAuthInfo(response.accessToken)
            maybeToken shouldBe defined
            maybeToken.get.userId shouldBe empty
            maybeToken.get.clientId shouldBe Some(clientId)
          }
        }

        "should not authorize client when credentials are wrong" in pending
        // TODO: negative scenarios
      }

      s"${P.GrantType} = ${P.AuthorizationCode}" - {
        "should provide authorization code by request" in {
          val (username, password)     = registerUser
          val (clientId, clientSecret) = registerClient
          val scope                    = Seq("a", "b").mkString(" ")
          val state = UUID.randomUUID.toString
          val accessToken              = authorizeUser(username, password)

          Post("/oauth/consent/approve").withEntity(
            FormData(
              P.AccessToken -> accessToken,
              P.ClientId -> clientId,
              P.RedirectUri -> appRedirectUri,
              P.Scope -> scope,
              P.State -> state).toEntity) ~>
            route ~> check {
            checkRedirect(StatusCodes.SeeOther, appRedirectUri)
            val parameters = redirectUriParameters
            val maybeCode = parameters.get(P.Code)
            maybeCode shouldBe defined
            parameters.get(P.State) shouldBe Some(state)
          }
        }


        "should not provide authorization code when client is invalid" in {
          val (username, password)     = registerUser
          val resp = requestAuthorizationCode(username, password, "invalidclientid")
          resp.get(P.Code) shouldBe empty
          resp.get(P.Error) shouldBe Some(P.UnauthorizedClient)
        }

        "should not provide authorization code when access token is invalid" in {
          val (clientId, _) = registerClient
          val scope                    = Seq("a", "b").mkString(" ")
          val state = UUID.randomUUID.toString
          val accessToken = "invalid"

          Post("/oauth/consent/approve").withEntity(
            FormData(
              P.AccessToken -> accessToken,
              P.ClientId -> clientId,
              P.RedirectUri -> appRedirectUri,
              P.Scope -> scope,
              P.State -> state).toEntity) ~>
            route ~> check {
            rejection shouldBe a[AuthenticationFailedRejection]
          }
        }

        "should not provide authorization code when user wasn't authenticated" in pending

        "should not provide authorization code when client id is invalid" in pending

        "should not provide authorization code when user was reject consent approve" in pending

        "should authorize client by authorization code" in {
          val (username, password)     = registerUser
          val (clientId, clientSecret) = registerClient
          val resp = requestAuthorizationCode(username, password, clientId)
          val maybeCode = resp.get(P.Code)
          maybeCode shouldBe defined

          Post("/oauth/token").withEntity(
            FormData(
              P.GrantType -> P.AuthorizationCode,
              P.Code -> maybeCode.get,
              P.ClientId -> clientId,
              P.ClientSecret -> clientSecret).toEntity) ~>
            route ~> check {
            status shouldBe StatusCodes.OK
            val response   = entityAs[AccessTokenResponse]
            val maybeToken = extractAuthInfo(response.accessToken)
            maybeToken shouldBe defined
            maybeToken.get.userId shouldBe defined
            maybeToken.get.clientId shouldBe Some(clientId)
          }
        }

        "should authorize client by authorization code with PKCE S250 code challenge" in {
          val (username, password)     = registerUser
          val (clientId, clientSecret) = registerClient
          val codeVerifier = base64Encoder.encodeToString(Random.nextString(10).getBytes)
          val s256Code = base64Encoder.encodeToString(sha256Digest.digest(codeVerifier.getBytes))
          val resp = requestAuthorizationCode(username, password, clientId,
            Seq(P.CodeChallengeMethod -> P.S256, P.CodeChallenge -> s256Code))
          val maybeCode = resp.get(P.Code)
          maybeCode shouldBe defined

          Post("/oauth/token").withEntity(
            FormData(
              P.GrantType -> P.AuthorizationCode,
              P.Code -> maybeCode.get,
              P.ClientId -> clientId,
              P.ClientSecret -> clientSecret,
              P.CodeVerifier -> codeVerifier).toEntity) ~>
            route ~> check {
            status shouldBe StatusCodes.OK
            val response   = entityAs[AccessTokenResponse]
            val maybeToken = extractAuthInfo(response.accessToken)
            maybeToken shouldBe defined
            maybeToken.get.userId shouldBe defined
            maybeToken.get.clientId shouldBe Some(clientId)
          }
        }

        "should authorize client by authorization code with PKCE Plain code challenge" in pending

        "should not authorize when authorization code is wrong" in {
          val (clientId, clientSecret) = registerClient

          Post("/oauth/token").withEntity(
            FormData(
              P.GrantType -> P.AuthorizationCode,
              P.Code -> "invalidcode",
              P.ClientId -> clientId,
              P.ClientSecret -> clientSecret,
            ).toEntity) ~>
            route ~> check {
            status shouldBe StatusCodes.Unauthorized
          }
        }

        "should not authorize when client secret is invalid" in {
          val (username, password)     = registerUser
          val (clientId, clientSecret) = registerClient
          val resp = requestAuthorizationCode(username, password, clientId)
          val maybeCode = resp.get(P.Code)
          maybeCode shouldBe defined

          Post("/oauth/token").withEntity(
            FormData(
              P.GrantType -> P.AuthorizationCode,
              P.Code -> maybeCode.get,
              P.ClientId -> clientId,
              P.ClientSecret -> "invalidsecret",
            ).toEntity) ~>
            route ~> check {
            status shouldBe StatusCodes.Unauthorized
          }
        }

        "should not authorize when code was expired" in pending

        "should not authorize when PKCE S250 code challenge failed" in pending

        "should not authorize when PKCE Plain code challenge failed" in pending
      }
    }
  }


  implicit class SyncExt[T](future: Future[T]) {
    def sync: T = Await.result(future, 5 seconds)
  }
}
