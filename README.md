# POC Implementation of OAuth2

The project implements proof of concept implementation of OAuth2 server.
It's not the complete production-ready solution. At least it's not yet.

## Supported features

- Registration of users with username and password credentials
- Registration of clients with a public key (RS256)
- Support of OAuth2 authorization code request with optional PKCE (plain or s256)
- Support of following OAuth2 grant types:
    - Authorization Code
    - Password
    - ClientCredentials


## Limitations

- At the moment it's pure API solution and the complete authorization code flow not yet provided: 
  this means that there is no login and consent approve pages.
  But there is consent approve endpoint which accepts the form data of consent approve page.
- There is no support of token refresh.
- Some error responses may not strictly follow the 
  [RFC-6749 - The OAuth 2.0 Authorization Framework](https://tools.ietf.org/html/rfc6749)
  
## Implementation Notes

### Authorization Code Flow

The complete Authentication code flow:
![Authorization Code Flow](/doc/images/AuthorizationCodeFlow.svg)

As mentioned above, at the moment it's pure API solution and 
the complete authorization code flow not yet provided: 
this means that there is no login and consent approve pages.

This implementation provides the consent approval API endpoint.
It should be called with authorization code request by the authenticated user.

#### Authorization Token Consent Approve Endpoint

- Request: 
    - Endpoint: `POST /oauth/consent/approve`
    - `Content-Type: application/form-data`
    - Data:
        - `access_token: String` — ueser's access token
        - `client_id: String` — client id
        - `redirect_uri: String` — callback uri
        - `scope: String` — scopes to authorize access
        - `code_challenge: String` — code for PKCE challenge
        - `code_challenge_method: String` — PKCE verification method "plain" or "s256".
    
- Response: redirect to `redirect_uri`
    - Query parameters:
        - `code: String` — authorization code
        - `error: String` — error type (in case of error).
    
#### Authorization Token Endpoint

The endpoint accepts `POST` requests with form data at `/oauth/token`.
  
There are several supported grant types with different request data.

- `grant_type`
    - `authorization_code` 
    - `password`
    - `client_secret`
  
All grant requests respond with the same response type:

- Response: 
  - `Content-Type: application/json`
  - Json data:
    - `access_token: String` — access token
    - `token_type: String` — Always `"Bearer"`
    - `expireAt: Number` — expiration time
    - `refresh_token: String` — refresh token (optional)


##### `authorization_code` grant type

- Request: 
    - Endpoint: `POST /oauth/token`
    - `Content-Type: application/form-data`
    - Data:
        - `grant_type=authorization_code`
        - `client_id: String` — client id
        - `client_secret: String` — client secret (can be omitted to authorize user)
        - `code: String` — authorization code
        - `code_verifier: String` — PKCE code verifier

##### `password` grant type

- Request: 
    - Endpoint: `POST /oauth/token`
    - `Content-Type: application/form-data`
    - Data:
        - `grant_type=password`
        - `client_id: String` — JWT token
        - `client_secret: String` — client secret (can be omitted to authorize user)
        - `username: String` — username
        - `password: String` — password
    
##### `client_secret` grant type

- Request: 
    - Endpoint: `POST /oauth/token`
    - `Content-Type: application/form-data`
    - Data:
        - `grant_type=client_secret`
        - `client_id: String` — client id
        - `client_secret: String` — client secret (can be omitted to authorize user)
        
The `client_secret` should be the JWT claim encoded by the Client's `privateKey`
with the `issuer=client_id` provided by client registration registration.

### User and Client API

#### User Registration

User registers in the system using `username` and `password`.

The passwords stored in system as salted Blowfish hashes. Two salts used in password hashing. 
- System wide permanent random string stored in the configuration parameter `password-store.salt`.
- Another salt generates for each password and stored together with the password hash.


- Request: 
    - Endpoint: `PUT /api/user`
    - `Content-Type: application/json`
    - Data:
        - `username: String` — username
        - `password: String` — password

- Response:
    - `Content-Type: application/json`
    - Data:
        - `id: String` - the ID of registered user

#### Client Registration API

![Client Registration and Authorization](/doc/images/ClientRegistrationAndAuthorization.svg)

The clients registers in system using the client's RSA public key. 
The server then able to authorize client by client's id encoded within JWT claim secret in the `issuer` parameter. 

- Request: 
    - Endpoint: `PUT /api/user`
    - `Content-Type: application/json`
    - Data:
        - `publicKey: String` — the client's public key

- Response:
    - `Content-Type: application/json`
    - Data:
        - `id: String` - the ID of registered client
  
## Project Structure


The all implementation now lives in `authorization-server` subdirectory.

- `OAuthProviderService` — The core of OAuth2 provider implementation. 
  It handles `AuthorizationCodeRequest` and `AccessTokenRequest` 
  to provide authorization.
  
- `OAuthProviderServer` — The OAuth2 provider server implementation on Akka HTTP.
  Provides the server binging and Akka HTTP route.
  
- `UserService` — contract on functionality of the basic user service. 
  Find and register users. Implemented by `UserServicePG`.
  
- `ClientService` — contract on functionality of the basic client service. 
  Find and register client. Implemented by `ClientServicePG`.

### Database

This projects uses the Postgresql database as primary data storage 
and the [Quill](https://getquill.io/) library as ORM solution.

[Flyway](https://flywaydb.org/) used for database generation and database schema versioning.
You can find the Flyway migration scripts under the `src/main/resources/db/migration` directory.

### Testing

The test can be run with the `sbt test` command.

- `OAuthProviderServerTest` — checks the main functionality of the
  `OAuthProviderServer` and `OAuthProviderService`. 
  The test implemented like integration test and uses the real 
  implementation of services without mocking. It uses test database 
  provided by `PostgresTestDatabaseProvider`. The database creates
  in the Postgresql docker container instance created by `DockerPostgresService`
  using Whisk's [docker-it-scala](https://github.com/whisklabs/docker-it-scala).
  
- `OAuthCommonDirectivesTest` — Check Akka HTTP directives defined in the `OAuthCommonDirectives`.

## Further improvements

### Feature improvements

#### Complete solution for authorization code request flow.

The code request flow how it should be:

1. The client — redirects the user agent to the authorization server for the code request.
1. The user agent — request authorization token by GET request.
1. Authorization server — should check user authorization (user's access token).
1. Authorization server — if the user not authenticated redirect to the authentication page.
1. The user — should authenticate himself by his credentials.
1. Authorization server — check authentication and redirect the user to the consent approve page.
1. The user — should approve the required scope or reject the client authorization request.
1. Authorization server — should check consent approval and redirect the user agent to the 
   redirect URI provided within the authorization request.
   
  [RFC-6749 - The OAuth 2.0 Authorization Framework](https://tools.ietf.org/html/rfc6749)

   
#### Modularity and flexibility

- At the moment the HTTP request parsing and HTTP response marshaling 
  highly rely on Akka HTTP, so the project is tightly coupled with Akka HTTP.
  It might be better to implement some common framework independent 
  `Request` and `Response` types to transport required HTTP 
  requests and responses data. 

  Another way is to implement an additional layer for handling HTTP requests 
  and provide the different implementations for different HTTP frameworks. 

- The current implementation of `OAuthProviderServer` tightly depends on 
  `UserService` and `ClientService`. Better to provide 
  some additional universal interface to handling data 
  related to users and clients.

#### Full OAuth2 specification support

- Improve error responses
- Implement access token refresh 

### Code Improvements

- Ihe implementation of `OAuthProviderService` uses the pure `Future[Result]` 
  and throws exceptions in Future context. It's not the best solution.
  Better solutions:
  - use ZIO with typed errors.
  - provide additional response types with success and error variants.
  - maybe use `Future[Either[Error, Result]]`, 
    but I not like pure `Either` inside the `Future` because it's not very handful 
    even with the `EitherT` from Cats.
  
- Improve tests. At the moment the tests implemented using Akka HTTP route teskit.
  I prefer to reimplement test as pure integration tests which 
  communicates with the running HTTP server by HTTP requests and responses.
  
- Provide more unit tests on internal components. 
  I prefer integration tests than pure unit tests with mocking 
  because it's allows to find hidden issues in the real system. 
  BTW it's reasonable to provide unit tests for some internal components.
  