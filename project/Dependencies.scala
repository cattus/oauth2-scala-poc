import sbt._

object Dependencies {
  val CirceVersion = "0.12.1"
  val EnumeratumVersion = "1.5.15"
  val ScalatestVersion = "3.0.8"
  val QuillVersion = "3.5.0"
  val AkkaVersion = "2.5.30"
  val AkkaHttpVersion = "10.1.11"
  val PureConfigVersion = "0.11.1"
  val JwtVersion = "4.2.0"
  val AkkaHttpCirce = "1.23.0"
  val TypesafeConfigVersion = "1.3.1"
  val FlywayVersion  = "6.3.2"
  val JBCryptVersion = "0.4"
  val SpotifyDockerClientVersion = "8.3.0"
  val WiskDockerTestkitVersion = "0.9.7"
  val MavwireVersion = "2.3.3"
  val Log4jScalaVersion = "11.0"
  val PostgresJdbcDriver = "42.2.11"
  val Log4jVersion = "2.13.1"
  val BouncyCastleVersion = "1.58"



  val scalaTest = "org.scalatest" %% "scalatest" % ScalatestVersion % "test"

  private[this] def akka(m: String) = "com.typesafe.akka" %% s"akka-$m" % AkkaVersion
  val akkaActor = akka("actor")
  val akkaActorTyped = akka("actor-typed")
  val akkaStream = akka("stream")
  val akkaTestKit = akka("testkit") % Test
  val akkaCoreBundle = Seq(akkaActor, akkaActorTyped, akkaStream, akkaTestKit)

  val akkaHttp = "com.typesafe.akka" %% "akka-http"   % AkkaHttpVersion
  val akkaHttpCirce = "de.heikoseeberger" %% "akka-http-circe" % AkkaHttpCirce
  val akkaHttpTestkit = "com.typesafe.akka" %% "akka-http-testkit" % AkkaHttpVersion % Test
  val akkaHttpBundle = Seq(akkaHttp, akkaHttpTestkit, akkaHttpCirce)

  val quill = "io.getquill" %% "quill-async-postgres" % QuillVersion
  val flywaydbCore = "org.flywaydb" % "flyway-core" % FlywayVersion
  val postgres = "org.postgresql" % "postgresql" % PostgresJdbcDriver
  val postgresBundle = Seq(quill, flywaydbCore, postgres)

  private[this] def circe(module: String) = "io.circe" %% s"circe-$module" % CirceVersion
  val circeBundle = Seq("core", "generic", "parser").map(circe)

  val enumeratum = "com.beachape" %% "enumeratum" % EnumeratumVersion
  val enumeratumCirce = "com.beachape" %% "enumeratum-circe" % EnumeratumVersion
  val enumeratumBundle = Seq(enumeratum, enumeratumCirce)

  val jwtCirce = "com.pauldijou" %% "jwt-circe" % JwtVersion
  val jbcrypt  = "org.mindrot" % "jbcrypt" % JBCryptVersion

  val typesafeConfig = "com.typesafe" % "config" % TypesafeConfigVersion
  val pureConfigCore = "com.github.pureconfig" %% "pureconfig" % PureConfigVersion
  val configBundle = Seq(typesafeConfig, pureConfigCore)

  val spotifyDockerClient = "com.spotify" % "docker-client" % SpotifyDockerClientVersion % "test"
  val wiskDockerTestkitCore = "com.whisk" %% "docker-testkit-core" % WiskDockerTestkitVersion % "test"
  val wiskDockerTestkitSpotify = "com.whisk" %% "docker-testkit-impl-spotify" % WiskDockerTestkitVersion % "test"

  val dockerTestBundle = Seq(spotifyDockerClient, wiskDockerTestkitSpotify, wiskDockerTestkitCore)

  val log4jApiScala = "org.apache.logging.log4j" %% "log4j-api-scala" % Log4jScalaVersion
  private def log4j(module: String) = "org.apache.logging.log4j" % s"log4j-$module" % Log4jVersion
  val loggingBundle = Seq("api", "core", "slf4j-impl", "1.2-api").map(log4j) :+ log4jApiScala

  val macwireBundle = Seq(
    "com.softwaremill.macwire" %% "macros" % MavwireVersion % "provided",
    "com.softwaremill.macwire" %% "util" % MavwireVersion)

   val bouncyCastle = "org.bouncycastle" % "bcpkix-jdk15on" % "1.58"
}
